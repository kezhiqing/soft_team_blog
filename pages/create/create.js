var app = getApp()
var DynID = new Array()
var UserID
var util = require('../../utils/util.js');
var startTime
var endTime
Page({
  data: {
    condition: false
  },

  onLoad: function (options) {
    DynID = null;
  },

  onShow: function (options) {
    var that = this;
    this.setData({
      condition: false
    })
    UserID = app.globalData.UserID;
    wx.request({
      url: 'https://ke.slickghost.com/beta/chooseActivity.php',
      data: {
        UserID: UserID
      },
      headers: {
        'Content-Type': 'application/json'
      },
      success: function (res) {
        //将获取到的json数据，存在名字叫array的这个数组中
        if (res.data != null) {
          that.setData({
            array: res.data
          })
        }
        else {
          array: 0;
        }
      }
    })
  },

  radioChange: function (e) {
    var id = e.detail.value;
    wx.request({
      url: 'https://ke.slickghost.com/beta/getTime.php',
      data: {
        id: id
      },
      success: function (res) {
        var condition = 0;
        startTime = res.data[0].StartTime;
        endTime = res.data[0].EndTime;
        var time = util.formatTime(new Date());
        var date = time.slice(0, 10);
        if (time < startTime) {
          wx.showModal({
            title: '活动未开始',
            content: '活动开始时间：' + startTime,
          })
        }
        else if (time > endTime) {
          wx.showModal({
            title: '活动已结束',
            content: '活动结束时间：' + endTime,
          })
        }
        else {
          wx.request({
            url: 'https://ke.slickghost.com/beta/limit.php',
            data: {
              ActID: id,
              UserID: UserID,
              date: date
            },
            success: function (res) {
              DynID = res.data;
              if (DynID != null) {
                wx.showModal({
                  title: '提示',
                  content: '该活动您今天已打过卡啦',
                })
              }
              else {
                wx.navigateTo({
                  url: '../release/release?dataid=' + id
                })
              }
            }
          })
        }
      }
    })
  }
})












