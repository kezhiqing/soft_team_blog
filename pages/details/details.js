var ActID
Page({
  onLoad: function (options) {
    var that = this;
    ActID = options.ActID;
    wx.request({
      url: 'https://ke.slickghost.com/beta/activityContent.php',
      method: 'GET',
      data: {
        ActID: ActID
      },
      headers: {
        'Content-Type': 'application/json'
      },
      success: function (res) {
        console.log(res.data);//将获取到的json数据，存在名字叫activity的这个数组中
        that.setData({
          activity: res.data
        })
      }
    })
  },

  onShow: function (e) {
    var that = this;
    wx.request({
      url: 'https://ke.slickghost.com/beta/allDynamic.php',
      data: {
        ActID: ActID
      },
      headers: {
        'Content-Type': 'application/json'
      },
      success: function (res) {
        console.log(res.data); //将获取到的json数据，存在名字叫array的这个数组中
        that.setData({
          arr: res.data
        })
      }
    })
  },
})