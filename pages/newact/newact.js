var util = require('../../utils/util.js');
var name
var content
var startTime
var endTime
var UserID
var time = util.formatTime(new Date())
//获取应用实例
var app = getApp()
Page({
  data: {
    startTime: time,
  },
  onShow: function (options) {
    name = null;
    content = null;
    startTime = null;
    endTime = null;
    UserID = app.globalData.UserID;
    this.setData({
      activityName: null,
      activityContent: null,
      dates1: '',
      dates2: ''
    })
  },

  bindKeyInput1: function (e) {
    name = e.detail.value;
    this.setData({
      activityName: e.detail.value
    })
  },

  bindKeyInput2: function (e) {
    content = e.detail.value
    this.setData({
      activityContent: e.detail.value
    })
  },

  bindDateChange1: function (e) {
    startTime = e.detail.value
    this.setData({
      dates1: e.detail.value
    })
  },

  bindDateChange2: function (e) {
    endTime = e.detail.value
    this.setData({
      dates2: e.detail.value
    })
  },

  new: function (e) {
    if (name == null || name == '' || content == null || content == '' || startTime == null || endTime == null) {
      wx.showModal({
        title: '提示',
        content: '不能为空！',
      })

    }
    else if (endTime <= startTime) {
      wx.showModal({
        title: '提示',
        content: '结束时间必须大于开始时间！',
      })
    }
    else {
      while (content.indexOf('\n') >= 0) {
        content = content.replace('\n', '<br>');
      }
      wx.showToast({
        title: '创建成功',
        icon: 'success',
        duration: 4000
      }),
        wx.request({
          url: 'https://ke.slickghost.com/beta/createActivity.php',
          data: {
            activityName: name,
            activityContent: content,
            startTime: startTime,
            endTime: endTime,
            UserID: UserID
          }
        }),
        wx.switchTab({
          url: '../index/index',
        })
    }
  }
})