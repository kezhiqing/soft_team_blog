var UserID
var ActID
var dynamicContent
var time
var app = getApp()
var util = require('../../utils/util.js')
Page({
  /*页面的初始数据*/
  data: {
    noteMaxLen: 70,//最多字数
    info: "",
    noteNowLen: 0,//当前字数
  },

  bindKeyInput2: function (e) {
    var that = this;
    dynamicContent = e.detail.value;
    var value = e.detail.value, len = parseInt(value.length);
    if (len > that.data.noteMaxLen) return;
    that.setData({
      info: value,
      noteNowLen: len
    })
  },

  listenerLogin: function (e) {
    if (dynamicContent == null || dynamicContent == '') {
      wx.showModal({
        title: '提示',
        content: '不能为空！',
      })
    } 
    else {
      while (dynamicContent.indexOf('\n') >= 0) {
        dynamicContent = dynamicContent.replace('\n', '<br>');
      }
      time = util.formatTime(new Date());
      wx.showToast({
        title: '发表成功',
        icon: 'success',
        duration: 4000
      }),
        wx.request({
          url: 'https://ke.slickghost.com/beta/deliver.php',
          data: {
            UserID: UserID,
            ActID: ActID,
            PunTime: time,
            PunCon: dynamicContent
          }
        }),
        wx.switchTab({
          url: '../index/index',
        })
    }
  },

  /*生命周期函数--监听页面加载*/
  onLoad: function (options) {
    var that = this;
    UserID = app.globalData.UserID;
    ActID = options.dataid;
    dynamicContent = null;
  },
})
